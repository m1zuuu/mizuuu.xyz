---
title: "I'm weak"
date: 2023-07-24
---

# I'm weak

I am spending my days alone right now, stuck in the matrix. Waking up at 05:30, going to work at 07:00, coming back home at 19:00 and going to bed before 22:00.

I realized I feel lonely. I feel unfullfilled. I feel though as there is something missing from my life. As much as I enjoying being a salesman and interacting with people, having wholesome conversations and restoring my faith in humanity that the stupid leftists aren't going to take over; I feel like a cog, haivng no clear aim, progression or improvements in life.

Living alone, truly alone seems to be a lot more different than I thought. The fact that I am in a hotel that by my standards isn't clean and the fact that I eat outside everyday; cold bread, rice and wraps everyday is slowly but truly getting to me. What I am having is not a struggle meal, and I am very thankful to fill my stomach everyday and drink clean water. But as a human, as a man I feel like my "power" is not increasing.

Time is passing, but my experiences are not growing. What I refer to as experiences isn't the interactions or things I do everyday, but points of growth in my life. Whether this be physical growth or learning something new. That's just not happening right now; and I know I can blame it on the fact that I have no internet or that I should've brought a book alongside with me. But these are all just fucking excuses.

At this point in time I have no clue what I can change while I do this job. But I feel like this has really been an awakening. Imagine being stuck in my life. No time, location or money freedom.
Eating the same fucking food, living in the same hotel in different locations and have the same fucking smile everyday as I talk to people. It sickens me. I am not saying that I do not enjoy my job. Being a salesman and learning how to appeal to customer is a journey I wanted to take and happy of it, but the thought of living life like this not only feels tiring but also unfullfilling to me. This is definetely not for me.

I love myself, but not enough.
