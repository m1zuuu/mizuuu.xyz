---
title: "The 100 hour rule"
date: 2023-05-07
---

# The 100 hour rule

They say if you spend 100 hours doing something, you'll be better than 95% of other humans.
You can spend 100 hours in a year by spending just 18 minutes everyday doing something.
It's also all about consistency. A lot of short bursts is better than occasional long bursts of
doing something.

This really turns the tables if you start thinking about it seriously this really motivates
you to think twice when go to "enjoy" your time by doing something unproductive.

I am going to start stretching for 10 minutes everyday from now on.
I will also try getting back into the habit of meditating and then maybe proceed
onto trying yogo as well.
