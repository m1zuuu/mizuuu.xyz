---
title: "Bottled up feelings"
date: 2024-10-27
---

It's been a quite a long time since I've actually reflected upon what I feel and how I feel about myself. To be honest its been a lot of positives - a bit too much if I really think about it. Of course compared to the dream state; I've got a lot of ground to cover in the next 2 months.

I think it's best to set myself some sort of acceptable state for the end of december that I need to reach. Its unforgivable if I cannot get shit sorted in a timeline. I need to start systemising processing what goes on in my head instead of just saying alri fuck that lets move on, cause it keeps building up, this is like the second time this year? like there was absolutely no need - how can I expect to take care of other people with stupid thoughts like these that serve no function. 

> I could just use something like my notes app to do this, but I choose to do it here. It's public and so easily accessible from any of my devices.

Thats why I am just going to dump a bunch of unorganised thoughts here for me to sort out next weekend.

# Thoughts dump

lmao so like i just feel like nothing but a loser. i have been trying hard enough for about 1 year now but have fucking nothing to show for all the sweat and tears shed. like wtf man. its seriously just sad to think about since im never gonna quit but just really unsatisfied with life atm. Nothing is changing despite me changing. im still in square one. poor, ugly and quite weak. pathetic.

but sometimes you're left with no choice but to trust the process and play the long game. looks like 1 year isnt enough. well guess what im gonna stay on it for 2 more years. volume > luck 100% of the time so i do NOT want to ever fucking quit cause it gets too hard. just like my mentality with gambling HAHAHA. only jk ofcourse, i do not yet have the funds to gamble.

I think given my delusion thinking, i am no where near handome, rich or strong enough to depict 1 years worth of progress. this means going forward i need to make up for it big time so by the end of next september - the whole playing field is different.

Same kinda situation when it comes to romantic relationships. I am meeting the same people everyday so how can I expect to meet someone. not only that but due to my sheer incompetence I am still not in a place where I can allow a relationship into my life.

It just bloody sucks cause i've been having episodes of affectionism but like got no bitches and don't really interact with my family so its quite hard to have an outlet for it. but that ends today with this post. cannot relax just yet, the jobs not done yet.

apart from that i actually feel like im having a bit too much fun with my friends at the moment. So I might start limiting that too. during my next academic semester i might acc just isolate myself a bit. i have great friends, a brother and a sister - i cannot keep letting them down. i have no clue what i did to deserve these great people around me, who have a lot more going on than me but deal with it like its nothing. then there's me who has to write a pathetic post about it to get over it.

i also have this really weird scenario with my family. I love them to bits, and have respect for them, but cannot deal with them at all. how is this even possible? I want to be able to do anything for them but i can never hang around for an extended period of time without getting annoyed or mad.

this also got me thinking maybe, i am just undeserving of love? but what makes a person deserving of love?(trying to describe it without using the word love). this really got me thinking - what a low point i must be to even talk about this. love both romantically and non-romantically is such a strange, mystical thing. for me trying to describe love is like trying to catch air. i cannnot just describe it through my words - its something that i feel really deep in me - i cannot change this or like have much say in it; it just happens.

Now as im talking about this im once again reminded of how i cannot articulate my thoughts in a meaningful way - its always a yap or a ramble that doesn't make any sense.

All in all, there is only one thing for me to continue doing - that is fixing the fuck up. I honestly feel sorry for people who know me. What use am I? literally 0. imagine. 0. 

I think whats important for me right now is to take these feelings, chew it and spit them the fuck out. I don't need this kinda negative thoughts in my mind, but yea there so much to unpack about my life. If anything im gonna expand on 'rate myself' which is a little note i made that goes through everything about me thats both good and bad right now. 

see u in the next one.
