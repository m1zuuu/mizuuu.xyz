+++
title = "Site inaguration!"
date = 2023-02-09
+++

Welcome to my brand new improved [site](https://mizuuu.xyz). Filled with rants and good times with my friends.
I treat this website as a my little corner in the internet where I can fill it with memories as an "archive" for later.


*I made this website as one way to jounral and imprint my website in history.
It contains very personal feelings and moments and I have it just as a way
to make my journalling more interesting.*

Here's to hoping that none of the people I know irl find this website !
